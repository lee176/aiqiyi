import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import OneHotEncoder
import joblib
# 加载数据
data = pd.read_csv("data.csv")

# 数据预处理，选择特征
features = ['tag', 'creator', 'contributor', 'title', 'year', 'month', 'bossStatus', 'hot_score']

# 将分类特征转换为字符串类型
for feature in ['tag', 'creator', 'contributor', 'title', 'bossStatus']:
    data[feature] = data[feature].astype(str)

X = data[features].fillna(data['hot_score'].mean())
y = data['score_per'].fillna(data['score_per'].mean())

# 对类别特征进行独热编码，设置 handle_unknown='ignore'
encoder = OneHotEncoder(handle_unknown='ignore')
X_encoded = encoder.fit_transform(X)

# 将独热编码后的结果转换为 DataFrame
X_encoded_df = pd.DataFrame(X_encoded.toarray(), columns=encoder.get_feature_names_out())

# 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X_encoded_df, y, test_size=0.2, random_state=42)

# 创建随机森林回归模型
model = RandomForestRegressor(n_estimators=100, random_state=42)

# 训练模型
model.fit(X_train, y_train)

# 保存模型
joblib.dump(model, 'randomForest.pkl')
joblib.dump(encoder, 'onehot_encoder.pkl')

model = joblib.load('randomForest.pkl')
# 预测
y_pred = model.predict(X_test)
print("预测结果:", y_pred)

# 计算均方误差
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)

# 对单个样本进行预测
sample = {'tag': '都市;爱情;剧情;生活;当代;内地',
          'creator': '',
          'contributor': "江疏影, 杨采钰, 张佳宁, 张慧雯, 李浩菲, 窦骁, 王安宇, 经超",
          'title': '抗日神剧',
          'year': 2024,
          'month': 3,
          'bossStatus': 'FREE',
          'hot_score': 6008}

# 将单个样本转换为 DataFrame
sample_df = pd.DataFrame([sample])

# 对特征进行预处理，例如独热编码
sample_encoded = encoder.transform(sample_df)

# 预测单个样本
sample_pred = model.predict(sample_encoded)
print("单个样本预测结果:", sample_pred)
