import requests
import pandas as pd

headers = {
    'accept': 'application/json',
    'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'origin': 'https://www.iqiyi.com',
    'referer': 'https://www.iqiyi.com/list/%E7%94%B5%E8%A7%86%E5%89%A7/%E7%BB%BC%E5%90%88_%E5%86%85%E5%9C%B0?',
    'cookie': 'QC005=6a486ce20bfea3234c058d1b74edd0b9; QC008=1710038721.1710038721.1710038721.1; QC175=%7B%22upd%22%3Atrue%2C%22ct%22%3A%22%22%7D; QC189=8212_A%2C5257_B%2C8004_C%2C6082_B%2C5335_B%2C5465_B%2C6843_B%2C6832_C%2C7074_C%2C7682_C%2C5924_D%2C6151_C%2C5468_B%2C8103_C%2C7024_C%2C5592_B%2C6031_B%2C6629_B%2C5670_B%2C6050_B%2C6578_B%2C6312_B%2C6091_B%2C6237_A%2C6249_C%2C7996_A%2C6704_C%2C6752_C%2C7332_B%2C7381_B%2C7423_C%2C7581_B%2C7760_B%2C8185_A; QC006=3f56813aa8700818edc574f25f656663; QP0037=0; QC186=false; TQC030=1; QC142=9b9c2b170b5c131e; QC191=; nu=0; __dfp=a1696fb1d412584cb9bee6cbd14803602cb173b1f2c332375f7ef18bf1a838db99@1711334722059@1710038723059; T00404=231f672ab0f6241d0a574ba314830c01; QP0034=%7B%22v%22%3A16%2C%22dp%22%3A1%2C%22dm%22%3A%7B%22wv%22%3A1%7D%2C%22m%22%3A%7B%22wm-vp9%22%3A1%2C%22wm-av1%22%3A1%2C%22m4-hevc%22%3A1%7D%2C%22hvc%22%3Atrue%7D; QP0030=1; QC218=5483379568764f9728be15385ad3e30c; QC220=cd650bc1ecdd10ead35a7b338198f5a3; QC219=bd4235d2580c4dbaba42572c5a7848a0; QC173=0; __oaa_session_referer_app__=dianshiju; QC187=true; QC192=%E7%BB%BC%E5%90%88%3B%E5%86%85%E5%9C%B0; P00004=.1710038739.3708984f4e; QC007=DIRECT; IMS=IggQABj_sbevBiouCiA5MDE2ZGQxOWM2OGVkODQ3OWU5ZTU2NDliNTQzMThlYxAAIggI0AUQAhiwCXIkCiA5MDE2ZGQxOWM2OGVkODQ3OWU5ZTU2NDliNTQzMThlYxAAggEAigEkCiIKIDczNDQ0MjAwZDdiZjFmZTIyOTQ4OWM3Yjc1OWJiOGJk; QC010=133115353; QP0036=2024310%7C110.524; websocket=true',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'
}

# url = "https://mesh.if.iqiyi.com/portal/videolib/pcw/data?version=1.0&ret_num=30000&page_id=1&device_id=6a486ce20bfea3234c058d1b74edd0b9&passport_id=&recent_selected_tag=%E7%BB%BC%E5%90%88%3B%E5%86%85%E5%9C%B0&recent_search_query=&scale=150&dfp=a1696fb1d412584cb9bee6cbd14803602cb173b1f2c332375f7ef18bf1a838db99&rl=1&channel_id=2&tagName=&mode=24&three_category_id_v2=8052642132978633"
url = """
https://mesh.if.iqiyi.com/portal/videolib/pcw/data?version=1.0&ret_num=30&page_id=1&device_id=6a486ce20bfea3234c058d1b74edd0b9&passport_id=&watch_list=8892506300,250,0&recent_selected_tag=%E7%BB%BC%E5%90%88%3B%E5%86%85%E5%9C%B0&recent_search_query=&scale=150&dfp=a1696fb1d412584cb9bee6cbd14803602cb173b1f2c332375f7ef18bf1a838db99&rl=1&channel_id=2&tagName=&mode=24
"""
url = """

https://mesh.if.iqiyi.com/portal/videolib/pcw/data?version=1.0&ret_num=30000&page_id=1&device_id=6a486ce20bfea3234c058d1b74edd0b9&passport_id=&watch_list=8892506300,250,0&recent_selected_tag=%E7%BB%BC%E5%90%88%3B%E5%86%85%E5%9C%B0&recent_search_query=&scale=150&dfp=a1696fb1d412584cb9bee6cbd14803602cb173b1f2c332375f7ef18bf1a838db99&rl=1&channel_id=2&tagName=&mode=24
"""

res_list = []

# 发送 GET 请求
r = requests.get(url, headers=headers)

# 输出响应状态码
print(r.status_code)

# 如果响应成功，尝试解析 JSON 数据
if r.status_code == 200:
    json_data = r.json()
    print(len(json_data["data"]))
    response = json_data["data"]
    # 数据解析
    for res_data in response:
        data = []
        order = res_data.get("order")
        desc = res_data["desc"]
        showDate = res_data["showDate"]
        tag = res_data["tag"]
        # 导演
        creator = [d['name'] for d in res_data["creator"]]
        # 演员
        contributor = [d['name'] for d in res_data['contributor']]
        firstChildBossStatusEnum = res_data.get('firstChildBossStatusEnum')
        dq_updatestatus = res_data['dq_updatestatus']
        bossStatus = res_data['bossStatus']
        tag_pcw = res_data.get('tag_pcw')
        title = res_data.get("display_name")
        hot_score = res_data.get('hot_score')
        data.append(order)
        data.append(desc)
        data.append(showDate)
        data.append(tag)
        data.append(creator)
        data.append(contributor)
        data.append(firstChildBossStatusEnum)
        data.append(dq_updatestatus)
        data.append(bossStatus)
        data.append(tag_pcw)
        data.append(title)
        data.append(hot_score)
        res_list.append(data)

else:
    print("请求失败，请检查网络连接和请求设置。")




def save(dataList):
    print(dataList)

    # 计算收视率

    df = pd.DataFrame(dataList, columns = ['order', 'desc', 'showDate', 'tag', 'creator', 'contributor',
                                 'firstChildBossStatusEnum', 'dq_updatestatus', 'bossStatus', 'tag_pcw', 'title', 'hot_score' ])

    # 提取年份和月份
    df['year'] = df['showDate'].apply(lambda x: x.split("-")[0])
    df['month'] = df['showDate'].apply(lambda x: x.split("-")[1])

    # 替换空字符串为 []
    df['creator'] = df['creator'].apply(lambda x: str(x).replace("[", "").replace("]", "").replace(",","，").replace("'",""))
    df['contributor'] = df['contributor'].apply(lambda x: str(x).replace("[", "").replace("]", "").replace(",","，").replace("'",""))

    scoreSum = df['hot_score'].sum()
    print(scoreSum)
    df['score_per'] = df['hot_score'].apply(lambda x: round(x/scoreSum*100, 2))
    print(df['score_per'].head(10))
    # df.to_excel('res.xlsx', index=False, engine='openpyxl')
    df.to_csv('data.csv', index=False)

save(res_list)