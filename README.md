# aiqiyi

#### 介绍
pyspark-aiqiyi

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

# 启动
start-dfs.sh
start-yarn.sh

nohup bin/hive --service metastore &
./hive



#创建表
CREATE TABLE aqy (
    `a_order` INT,
    `a_desc` STRING,
    `showDate` DATE,
    `tag` STRING,
    `creator` STRING,
    `contributor` STRING,
    `firstChildBossStatusEnum` STRING,
    `dq_updatestatus` STRING,
    `bossStatus` STRING,
    `tag_pcw` STRING,
    `title` STRING,
    `hot_score` INT,
     year INT,
     month INT,
     `score_per` DOUBLE
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE;

# 导入文件
LOAD DATA LOCAL INPATH '/root/data.csv' OVERWRITE INTO table aqy;

select * from aqy where a_order is not null limit 10;


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
